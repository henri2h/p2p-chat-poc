#pragma once

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "def.h"
#include "peer.h"
#include "random.h"

#define PORT 2000

#define BUF_LEN 256

#define FILE_NAME_ID "id"

void *listen_for_clients(void *data);
void *presence_broadcaster(void *data);

int save_device_id(DeviceIdentifier *id);
int create_or_load_device_id(DeviceIdentifier *id);
