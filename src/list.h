#ifndef LIST_H
#define LIST_H

#include <stdlib.h>

typedef struct ListItem {
  void *data;
  struct ListItem *next;
} ListItem;

typedef struct {
  ListItem *head;
  ListItem *tail;
  int count;
} List;

List *create_list();

void add_item(List *list, void *data);
void delete_item(List *list, int pos);

#endif
