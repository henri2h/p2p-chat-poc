#ifndef UI_H
#define UI_H

#include <ncurses.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include "chat.h"
#include "data.h"
#include "def.h"
#include "list.h"
#include "peer.h"
#include "term.h"

void peers_display_list(WINDOW *win, List *peers);
void run_ui(Data *data);

#endif
