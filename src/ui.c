#include "ui.h"

void peers_display_list(WINDOW *win, List *peers) {
  wclear(win);

  char mtime[40];

  int cursor = 1;
  mvwprintw(win, cursor, 1, "Peers\n");

  cursor++;
  cursor++;

  ListItem *lp = peers->head;
  int pos = 0;

  while (lp != NULL) {
    Peer *p = lp->data;

    if (p->created) {

      struct tm *lc = localtime(&p->last_seen);
      strftime(mtime, 40, "%H:%m:%S", lc);

      mvwprintw(win, cursor, 1, "Peer : %s", p->name);
      mvwprintw(win, cursor + 1, 1, "%lX", p->id);
      mvwprintw(win, cursor + 2, 1, "%s:%d", p->addr, p->listening_port);
      mvwprintw(win, cursor + 3, 1, "Last seen on %s", mtime);
      cursor += 5; // 5 items + 1 line space
    }

    lp = lp->next;
    pos++;
  }

  time_t t = time(NULL);
  struct tm *lc = localtime(&t);
  strftime(mtime, 40, "%H:%m:%S", lc);
  mvwprintw(win, LINES - 3, 1, "%s", mtime);
  mvwprintw(win, LINES - 2, 1, "%d peers", pos);

  box(win, ACS_VLINE, ACS_HLINE);
  wrefresh(win);
}

void display_messages(WINDOW *win, Data *data) {
  wclear(win);
  ListItem *mItem = data->messages->head;
  int pos = 0;

  if (mItem == NULL) {
    wprintw(win, "Welcome to our chat client\n");
  } else
    while (mItem != NULL) {
      Message *m = (Message *)mItem->data;
      Peer *p = peer_find(data->peers, m->sender_id);

      char name[30];

      if (m->sender_id == data->id->id) {
        strcpy(name, "you");
      } else if (p != NULL) {
        strcpy(name, p->name);
      } else {
        // in case we don't know this peer yet
        sprintf(name, "%lX", m->sender_id);
      }

      char mtime[40];

      struct tm *lc = localtime(&m->send_time);
      strftime(mtime, 40, "%A, %B %d %Y %H:%m:%S", lc);

      if (m->message_type == MESSAGE_TYPE_TEXT) {
        wprintw(win, "%s : %s : %s\n", mtime, name, m->content);
      } else if (m->message_type == MESSAGE_TYPE_ROOM_INFO) {
        wprintw(win, "%s : %s %s\n", mtime, name, m->content);
      } else {
        wprintw(win, "%lld : %s : %s ! unknown message type ! : %s\n",
                (long long)m->send_time, ctime(&m->send_time), name,
                m->content);
      }

      pos++;
      mItem = mItem->next;
    };
  wrefresh(win);
}

void display_message(WINDOW *w, WINDOW *panel_message_out, char *message) {
  mvwprintw(w, 1, 1, "message > %s", message);
  box(panel_message_out, ACS_VLINE, ACS_HLINE);

  wrefresh(panel_message_out);
  wrefresh(w);
}

void run_ui(Data *data) {

  WINDOW *panel_left, *panel_main, *panel_main_out, *panel_message,
      *panel_message_out;

  // nodelay(stdscr, TRUE); if we want to enable the no delay mode
  int init = 1;

  char message[MESSAGE_CONTENT_LEN] = "";
  int pos_test = 0;

  int cursor_pos_x = 11;
  int cursor_pos_y = 1;

  int mode = 1;
  int run = 1;

  while (run) {

    if (init) {
      initscr();
      clear();

      keypad(stdscr, TRUE);

      panel_left = subwin(stdscr, LINES, COLS / 8, 0,
                          0); // Créé une fenêtre de 'LINES / 2' lignes et de
                              // COLS colonnes en 0, 0

      panel_main = subwin(stdscr, LINES - 7, COLS * 7 / 8 - 2, 1, COLS / 8 + 1);
      panel_main_out = subwin(stdscr, LINES - 5, COLS * 7 / 8, 0,
                              COLS / 8); // Créé la même fenêtre que ci-dessus
                                         // sauf que les coordonnées changent

      panel_message =
          subwin(stdscr, 3, (COLS * 7 / 8) - 2, LINES - 5, COLS / 8 + 1);
      panel_message_out = subwin(stdscr, 5, COLS * 7 / 8, LINES - 5, COLS / 8);

      box(panel_main_out, ACS_VLINE, ACS_HLINE);
      box(panel_left, ACS_VLINE, ACS_HLINE);
      box(panel_message_out, ACS_VLINE, ACS_HLINE);

      mvwprintw(panel_left, 1, 1, "Ceci est la fenetre du haut");
      mvwprintw(panel_main, 1, 1, "Main window");
      mvwprintw(panel_message, 1, 1, "message > ");

      wrefresh(panel_main_out);
      wrefresh(panel_main);

      wrefresh(panel_left);

      display_message(panel_message, panel_message_out, message);

      noecho();
      cbreak();

      scrollok(panel_message, TRUE);
      scrollok(panel_main, TRUE);

      timeout(1000);
      init = 0;
    }

    // display UI
    if (mode == 1) {
      display_messages(panel_main, data);
      peers_display_list(panel_left, data->peers);
    }

    wmove(panel_message, cursor_pos_x, cursor_pos_y);
    wrefresh(panel_message);
    int c = getch();
    if (c != -1) {
      if (c == 127 || c == KEY_BACKSPACE) {
        if (pos_test > 0) {
          wclear(panel_message);

          message[pos_test] = 'n';
          pos_test--;
          message[pos_test] = '\0';
        }
      } else if (c == 10) {
        // send message
        ListItem *l = data->client_connection_threads->head;

        Message *m = createMessage(data->id->id, message);

        add_item(data->messages, m);

        while (l != NULL) {
          ClientConnectionThread *c = l->data;
          if (c->enabled) // check if the client is connected
          {
            write(c->socket, m, sizeof(Message));
          }
          l = l->next;
        }

        pos_test = 0;
        message[pos_test] = '\0';
        wclear(panel_message);
      } else if (c == KEY_RESIZE) {
        init = 1;
      } else {
        message[pos_test] = c;
        pos_test++;
        message[pos_test] = '\0';
      }

      display_message(panel_message, panel_message_out, message);
    }
  }
}
