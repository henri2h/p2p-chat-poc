#ifndef CHAT_H
#define CHAT_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "random.h"

#define MAX_LIGNES_IN 50
#define MESSAGE_CONTENT_LEN 1024

#define MESSAGE_TYPE_TEXT 0
#define MESSAGE_TYPE_ROOM_INFO 1

typedef struct {
  uint64_t sender_id;
  uint64_t id;
  int content_size;
  int message_type;
  time_t send_time;
  char content[MESSAGE_CONTENT_LEN];
} Message;

Message *createMessage(uint64_t sender_id, char *text);
#endif