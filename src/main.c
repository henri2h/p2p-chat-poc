#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "chat.h"
#include "client_discovery.h"
#include "data.h"
#include "def.h"
#include "list.h"
#include "listener.h"
#include "peer.h"
#include "ui.h"

#define THREADS_COUNT 3

int main(int argc, char *argv[]) {
  // init rand
  srand(time(NULL));

  // create data
  Data *data = createData();

  // try to load the device id.
  // If not, create one and ask for a name
  if (create_or_load_device_id(data->id)) {

    data->id->id = generateRandomID();

    printf("Client name : ");
    scanf("%s", data->id->name);

    // and save it
    save_device_id(data->id);
  }

  data->id->port = generateRandomPort();

  pthread_t threads[THREADS_COUNT];

  // chat system
  pthread_create(&threads[3], NULL, connection_listener, (void *)data);

  sem_wait(
      data->connection_listener_launched_sem); // wait for the client listener
                                               // to be launched (be sure to
                                               // have the right port)

  // discovery
  pthread_create(&threads[0], NULL, listen_for_clients, (void *)data);
  pthread_create(&threads[1], NULL, presence_broadcaster, (void *)data->id);

  printf("Created listener\n");

  // sync

  // display messages

  run_ui(data);

  // wait for all
  for (int i = 0; i < THREADS_COUNT; i++) {
    pthread_join(threads[i], NULL);
  }
}
