#include "client_discovery.h"

void *presence_broadcaster(void *data) {
  struct sockaddr_in addr;
  int fd;

  // get device id
  DeviceIdentifier *id = data;

  if ((fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
    perror("socket");
    exit(1);
  }

  int allow_broadcast = 1;
  setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (void *)&allow_broadcast,
             sizeof(allow_broadcast));

  /* set up destination address */
  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
  addr.sin_port = htons(BROADCAST_PORT);

  // send message

  int run = 1;

  while (run) {
    if (sendto(fd, id, sizeof(DeviceIdentifier), 0, (struct sockaddr *)&addr,
               sizeof(addr)) < 0) {
      perror("sendto");
      exit(1);
    }
    sleep(5);
  }
  return 0;
}

int save_device_id(DeviceIdentifier *id) {
  FILE *f;
  f = fopen(FILE_NAME_ID, "w");
  if (f == NULL) {
    printf("Could not open config file '%s'\n", FILE_NAME_ID);
    return 0;
  }
  fwrite(id, sizeof(DeviceIdentifier), 1, f);
  printf("[ DeviceIdentifier ] Config saved\n");
  fclose(f);
  return 1;
}

// load or create a DeviceIdentifier. Return true if it has been created.
int create_or_load_device_id(DeviceIdentifier *id) {
  id->port = 0; // to save

  int file_not_exists = access(FILE_NAME_ID, F_OK);
  if (file_not_exists == 0) {
    // file exists
    FILE *f;
    f = fopen(FILE_NAME_ID, "r");
    if (f != NULL) {
      fread(id, sizeof(DeviceIdentifier), 1, f);
      fclose(f);

      printf("[ DeviceIndentifier ] Config loaded\n");
      return 0;
    }
    printf("[ DeviceIndentifier ] Could not open file.  Loading new config "
           "file.\n");
  }

  return 1;
}

void *listen_for_clients(void *data_in) {
  Data *data = data_in;
  List *peers = data->peers;
  struct sockaddr_in si_me, si_other;

  int s;

  DeviceIdentifier *id_buf = malloc(sizeof(DeviceIdentifier));

  socklen_t slen = sizeof(si_other);

  s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (s == -1) {
    perror("socket");
    exit(1);
  }

  // allow multiple sockets to use the same PORT number

  int allow_socket_reuse = 1;
  setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&allow_socket_reuse,
             sizeof(allow_socket_reuse));

  memset(&si_me, 0, sizeof(si_me));
  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(BROADCAST_PORT);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);

  if (bind(s, (struct sockaddr *)&si_me, sizeof(si_me)) == -1) {
    printf("[ DeviceDiscovery ] could not bind\n");
    perror("bind");
    exit(1);
  }

  while (1) {
    int blen = recvfrom(s, id_buf, sizeof(DeviceIdentifier), 0,
                        (struct sockaddr *)&si_other, &slen);
    if (blen == -1) {
      printf("Could not recieve\n");
      perror("recvfrom");
      exit(1);
    } else {
      uint64_t id = id_buf->id;
      // detect if discovered peer has already been discovered
      // and don't connect to ourself
      if (id != data->id->id) {
        if (peer_exists(peers, id) == 0) {
          if (peers->count < PEER_MAX_COUNT) {

            Peer *peer = peer_create();
            strcpy(peer->name, id_buf->name);

            peer->id = id_buf->id;
            peer->listening_port = id_buf->port;
            peer->last_seen = time(NULL);
            peer->addr = inet_ntoa(si_other.sin_addr);
            peer->listening_port = id_buf->port;

            add_item(peers, peer);

            if (peer_is_connected(data, peer) == 0) {
              peer_connect(data, peer);
            }
          } else {
            printf("Maximum number of client reached\n");
          }
        } else {
          // We should update presence
          Peer *peer = peer_find(peers, id);
          peer->last_seen = time(NULL);
        }
      }
    }
  }

  close(s);
  return 0;
}