#ifndef PEER_H
#define PEER_H

#include <inttypes.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "connection_threads.h"
#include "data.h"
#include "list.h"

typedef struct {
  char *addr;
  int listening_port;

  time_t last_seen;
  int created;

  // identity
  uint64_t id;
  char name[30];

  pthread_t thread; // connection
  int socket;
} Peer;

Peer *peer_create();

Peer *peer_find(List *peers, uint64_t id);
int peer_exists(List *peers, uint64_t id);

int peer_is_connected(Data *data, Peer *p);
void peer_connect(Data *data, Peer *p);

#endif