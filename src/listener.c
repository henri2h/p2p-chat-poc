#include "listener.h"

void *connection_listener(void *d) {
  Data *data = (Data *)d;

  struct sockaddr_in adr_srv;

  int soc;

  // if one port is used, we are going to try to use the next one
  // we limit ourselves to 30 trials
  int ret = -1;
  int trials = 0;
  while (ret < 0) {
    soc = socket(AF_INET, SOCK_STREAM, 0);
    if (soc < 0) {
      printf("socket");
      exit(0);
    }

    memset(&adr_srv, 0, sizeof(adr_srv));
    adr_srv.sin_family = AF_INET;
    adr_srv.sin_port = htons(data->id->port);
    adr_srv.sin_addr.s_addr = INADDR_ANY;

    ret = bind(soc, (struct sockaddr *)&adr_srv, sizeof(struct sockaddr_in));
    if (trials > 30) {
      perror("bind");
      exit(0);
    }

    if (ret < 0) {
      trials++;
      data->id->port++;
      printf("[ listener ] Port %d in use, trying next one\n", data->id->port);
    }
    trials++;
  }
  printf("[ listener ] Binding to %d\n", data->id->port);

  int ret_listen = listen(soc, LISTENER_MAX_CLIENTS);
  if (ret_listen) {
    perror("listen");
    exit(0);
  }

  int listeningConnection = 1;
  sem_post(data->connection_listener_launched_sem);

  while (listeningConnection) {
    unsigned int lgAdrClient = sizeof(struct sockaddr_in);
    struct sockaddr_in *adrClient = malloc(lgAdrClient);

    // we are listening for incomming connections
    int canal = accept(soc, (struct sockaddr *)adrClient, &lgAdrClient);

    if (canal < 0)
      perror("accept");
    else {
      ClientConnectionThread *new_client = launchConnection(data, canal);
      sem_wait(data->client_connection_threads_sem);
      add_item(data->client_connection_threads, new_client);
      sem_post(data->client_connection_threads_sem);
    }
  }
  if (close(soc) == -1)
    printf("Error close ecoute");

  pthread_exit(NULL);
}
