#include "data.h"

Data *createData() {
  Data *data = malloc(sizeof(Data));

  data->id = malloc(sizeof(DeviceIdentifier));

  data->connection_listener_launched_sem = malloc(sizeof(sem_t));
  sem_init(data->connection_listener_launched_sem, 0, 0);

  data->peers = create_list();
  data->messages = create_list();

  data->client_connection_threads_sem = malloc(sizeof(sem_t));
  sem_init(data->client_connection_threads_sem, 0, 1);
  data->client_connection_threads = create_list();

  return data;
}