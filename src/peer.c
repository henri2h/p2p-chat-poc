#include "peer.h"

void peer_init(Peer *peer) {
  peer->addr = 0;
  peer->last_seen = 0;
  peer->created = 0;
  peer->socket = -1;
  peer->listening_port = 0;
  peer->id = 0;
  strcpy(peer->name, "");
}

Peer *peer_create() {
  Peer *peer = malloc(sizeof(Peer));

  peer_init(peer);
  peer->created = 1;

  return peer;
}

Peer *peer_find(List *peers, uint64_t id) {
  ListItem *li = peers->head;

  while (li != NULL) {
    Peer *p = li->data;
    if (p->id == id)
      return p;
    li = li->next;
  }
  return NULL;
}

int peer_exists(List *peers, uint64_t id) {
  if (peer_find(peers, id) != NULL)
    return 1;
  return 0;
}

int peer_is_connected(Data *data, Peer *p) {
  ListItem *li = data->client_connection_threads->head;

  while (li != NULL) {
    ClientConnectionThread *cThread = li->data;

    if (cThread->clientID == p->id && cThread->enabled)
      return 1;

    li = li->next;
  }

  return 0;
}

void peer_connect(Data *data, Peer *p) {
  p->socket = socket(AF_INET, SOCK_STREAM, 0);
  struct sockaddr_in addr;

  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = inet_addr(p->addr);
  addr.sin_port = htons(p->listening_port);

  if (connect(p->socket, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) <
      0) {
    printf("Connection failed due to port and ip problems\n");
  } else {
    sem_wait(data->client_connection_threads_sem);
    ClientConnectionThread *new_client = launchConnection(data, p->socket);
    add_item(data->client_connection_threads, new_client);
    sem_post(data->client_connection_threads_sem);
  }
}