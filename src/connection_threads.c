#include "connection_threads.h"

ClientConnectionThread *create_client() {
  ClientConnectionThread *client = malloc(sizeof(ClientConnectionThread));
  client->enabled = 0;
  client->thread = malloc(sizeof(pthread_t));
  client->socket = -1;
  client->clientID = -1; // not yet set

  return client;
}

typedef struct {
  ClientConnectionThread *c;
  Data *d;
} ClientListenerArguments;

void *listen_connection(void *args_in) {
  ClientListenerArguments *args = args_in;

  Data *data = args->d;
  ClientConnectionThread *c = args->c;

  // we are ready to enable the client
  c->enabled = 1;

  write(c->socket, &data->id->id, sizeof(data->id->id));
  read(c->socket, &c->clientID, sizeof(c->clientID));

  // add login message
  Message *m = createMessage(c->clientID, "connected");
  m->message_type = MESSAGE_TYPE_ROOM_INFO;
  add_item(data->messages, m);

  while (c->enabled) {
    Message *m = malloc(sizeof(Message));
    int n = read(c->socket, m, sizeof(Message));

    if (n > 0) {
      add_item(data->messages, m);
    } else {
      c->enabled = 0;
    }
  }

  // disconnected message
  m = createMessage(c->clientID, "disconnected");
  m->message_type = MESSAGE_TYPE_ROOM_INFO;
  add_item(data->messages, m);

  if (close(c->socket) == -1)
    printf("Error close canal");

  return NULL;
}

ClientConnectionThread *launchConnection(Data *data, int socket) {

  ClientConnectionThread *new_client = create_client();
  new_client->socket = socket;

  ClientListenerArguments *args = malloc(sizeof(ClientListenerArguments));
  args->c = new_client;
  args->d = data;

  pthread_create(new_client->thread, NULL, listen_connection, (void *)args);

  return new_client;
}