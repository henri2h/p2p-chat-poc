#include "list.h"

List *create_list() {
  List *list = malloc(sizeof(List));
  list->head = NULL;
  list->tail = NULL;
  list->count = 0;
  return list;
}

void add_item(List *list, void *data) {
  ListItem *ls = malloc(sizeof(ListItem));
  ls->next = NULL;
  ls->data = data;

  if (list->tail == NULL) {
    list->tail = ls;
    list->head = ls;
  } else {
    list->tail->next = ls;
    list->tail = ls;
  }
  list->count++;
}

// delete an item of the list at a specified index
// crash if attempt to delete a non existing item
void delete_item(List *list, int index) {
  list->count--;

  ListItem *posItem = list->head;
  int pos = 0;
  if (list->count == 0) {
    list->head = NULL;
    list->tail = NULL;
    free(posItem);
    return;
  }

  if (index == 0) {
    list->head = posItem->next;
    return;
  }

  while (posItem != NULL) {
    pos++;
    if (index == pos) {
      if (posItem->next->next != NULL) {
        ListItem *oldItem = posItem->next;
        posItem->next = posItem->next->next;
        free(oldItem);
        return;
      } else {
        // for the last item
        posItem->next = NULL;
        list->tail = posItem;
      }
      return;
    }
    posItem = posItem->next;
  }
}
