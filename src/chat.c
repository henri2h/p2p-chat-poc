#include "chat.h"

Message *createMessage(uint64_t sender_id, char *text) {
  Message *m = malloc(sizeof(Message));

  m->sender_id = sender_id;

  strcpy(m->content, text);
  m->content_size = MESSAGE_CONTENT_LEN;
  m->id = generateRandomID();
  m->message_type = MESSAGE_TYPE_TEXT;

  m->send_time = time(NULL);

  return m;
}
