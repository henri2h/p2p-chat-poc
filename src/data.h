#pragma once

#include <semaphore.h>
#include <stdint.h>
#include <stdlib.h>

#include "list.h"

typedef struct {
  uint64_t id;
  int port;
  char name[30];
} DeviceIdentifier;

typedef struct {
  sem_t *connection_listener_launched_sem;
  sem_t *client_connection_threads_sem;

  List *peers;
  List *messages;
  List *client_connection_threads;

  DeviceIdentifier *id;

} Data;

Data *createData();