#pragma once

#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "chat.h"
#include "data.h"

typedef struct {
  int enabled;
  uint64_t clientID;
  int socket;
  pthread_t *thread;
} ClientConnectionThread;

ClientConnectionThread *create_client();
void *listen_connection(void *data);

ClientConnectionThread *launchConnection(Data *data, int socket);