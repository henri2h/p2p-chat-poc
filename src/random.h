#pragma once

#include <inttypes.h>
#include <stdlib.h>
#include <time.h>

int generateRandomPort();
uint64_t generateRandomID();