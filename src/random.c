#include "random.h"

uint64_t generateRandomID() {
  uint64_t t;
  // create random id
  t = rand();
  return (t << 32) | rand();
}

int generateRandomPort() {
  time_t t;
  srand((unsigned)time(&t));
  return 2000 + rand() % 100;
}