#ifndef LISTENER_H
#define LISTENER

#include <arpa/inet.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "data.h"
#include "def.h"

#include "chat.h"
#include "connection_threads.h"
#include "list.h"

#define LISTENER_MAX_CLIENTS 10

void *connection_listener(void *data);
void listener_create(Data *data);

#endif