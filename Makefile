CC = gcc

SRCS := $(wildcard src/*.c)
OBJS := $(SRCS:src/%.c=build/%.o)
BIN = main

all : $(BIN)
	@echo All done

ncurses:
	scripts/compile_ncurses.sh

build:
	mkdir build

$(BIN) : $(OBJS)
	$(CC) $(OBJS) -Wall -o $(BIN) -lm -lpthread -Llib/ncurses/lib -lncurses

build/%.o : src/%.c | build
	$(CC) -g -Wall $< -c -o $@ -Ilib/ncurses

clean :
	@rm -r -f build
	@rm -f $(BIN)
	@echo Project cleaned