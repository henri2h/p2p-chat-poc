# Peer to peer chat

**Disclaimer**  This project is a proof of concept of a p2p chat system. This was done as a school project and may not be continued in the future.

For project like this one, one can head to [https://github.com/matrix-org/pinecone](https://github.com/matrix-org/pinecone). This project is an experimental p2p server for MATRIX. 

## Compiling

Ce projet utilise la librairie graphique “ncurses” pour permettre un affichage plus aisé des messages.

> sudo apt install libncurses-dev

Pour compiler l’application :
> make

Pour l’exécuter
> ./main

Un script pour lancer plusieurs fois le programme avec un nom de client différent est disponible
> ./demo.sh


Attention : au démarrage, le programme créer un fichier ‘id’ qui sert à enregistrer le  nom et l’id du client. Une fois générés, le nom et l’id ne seront plus modifiables, sauf utilisation de la commande :

>rm id
